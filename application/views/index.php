<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to Challenge Aivo</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>

	<div class="container">
		<h1>Welcome to Challenge Aivo!</h1>

		<form action="<?php echo base_url() ?>albums" method="get" accept-charset="utf-8">
			<div class="form-group">
				<label for="q">Ingrese el nombre del artista</label>
				<input type="text" class="form-control" id="q" name="q" placeholder="Ingrese el nombre del artista">
			</div>
			<button type="submit" class="btn btn-default">Buscar</button>
		</form>
	</div>
</body>
</html>