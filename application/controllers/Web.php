<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'third_party/vendor/autoload.php';

class Web extends CI_Controller {
	private $client_id;
	private $token;

	public function __construct() {
		parent::__construct();

		$this->client_id = '';
		$this->token = 'BQDr1y3FLkyPlJhx4QjevrFrZhn1KwfJ7bHhZOJREnws_5WHl2TwFjllGjKx8b8V8FAZBA6H9MaiNAX059zuFMGgEvuvDDxOVSjllA7E3Y_DCNMNtG8CN8u2gJ7oY2AgrvBRSlReAYTTFK4gnOzpaQ';
	}

	public function index() {
		$this->load->view('index');
	}

	public function login() {
		Requests::register_autoloader();

		$options= array(
		   'client_id' => $this->client_id,
		   'response_type' => 'code',
		   'redirect_uri' => base_url().'login/callback',
		   'state' => 'RFC-6749',
		   'scope' => 'user-read-private user-read-email',
		   'show_dialog' => false
		);
		$headers = array();
		$url = 'https://accounts.spotify.com/authorize';
		$request = Requests::get($url, $headers, $options);
	}

	public function loginCallback() {
		$sesion_data = array('spotify_token' => $this->token);

		if ($this->input->get('code')) {
			$sesion_data = array('spotify_token' => $this->input->get('code'));
		}

		$this->session->set_userdata($sesion_data);
		redirect(base_url(),'refresh');
	}

	public function getAlbums() {
		if (!$this->session->userdata('spotify_token') || !$this->input->get('q')) redirect(base_url(), 'refresh');

		Requests::register_autoloader();

		$headers = array(
			'Accept' => 'application/json',
			'Content-Type' => 'application/json',
			'Authorization' => 'Bearer '.$this->token
		);
		$url = 'https://api.spotify.com/v1/search?q='.$this->input->get('q').'&type=album';
		$request = Requests::get($url, $headers, array());

		$result = json_decode($request->body);
		$albums = $result->albums->items;

		$items = array();

		foreach ($albums as $f) {
			$data = array(
				'artist' => $f->artists[0]->name,
				'name' => $f->name,
				'released' => $f->release_date,
				'tracks' => $f->total_tracks,
				'cover' => array(
					'height' => $f->images[0]->height,
					'width' => $f->images[0]->width,
					'url' => $f->images[0]->url
				)
			);

			$items[] = $data;
		}

		echo json_encode($items);
	}
}